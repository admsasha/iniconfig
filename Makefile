CC=g++

CXXFLAGS += -c -std=c++11
CXXFLAGS += -pedantic -pedantic-errors
CXXFLAGS += -Wall -Wextra -Wformat -Wformat-security -Wno-unused-variable -Wno-unused-parameter

LDFLAGS=  -lgtest

SOURCES=main.cpp IniConfig.cpp
OBJECTS=$(SOURCES:.cpp=.o)

EXECUTABLE=test_iniconfig


all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) $(LDFLAGS)  -o $@

.cpp.o:
	$(CC) $(CXXFLAGS) $< -o $@

clean:
	rm -rf *.o $(EXECUTABLE)

