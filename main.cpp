#include "IniConfig.h"

#include <gtest/gtest.h>



TEST( testIni, testValidKeyString ) {
    IniConfig config("test.ini");
    EXPECT_EQ( config.getValueStr("section1","test","a"),  "111"  );
}

TEST( testIni, testValidDefaultKey ) {
    IniConfig config("test.ini");
    EXPECT_EQ( config.getValueStr("section1","key_non_exist","default_value"),  "default_value"  );
}


TEST( testIni, testValidKeyInt ) {
    IniConfig config("test.ini");
    EXPECT_EQ( config.getValueInt("section1","test2",0),  444  );
}

TEST( testIni, testExistGroup ) {
    IniConfig config("test.ini");
    ASSERT_TRUE(config.isExistSection("section2"));
}

TEST( testIni, testNonExistGroup ) {
    IniConfig config("test.ini");
    ASSERT_FALSE(config.isExistSection("section_non_exist"));
}

int main( int argc, char **argv ) {
    ::testing::InitGoogleTest( &argc, argv );
    return RUN_ALL_TESTS( );
}
